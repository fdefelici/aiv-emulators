#include "../include/mos6502.h"

static int adc_immediate(mos6502_t *cpu) {
    unsigned char value = cpu->read8(cpu, cpu->pc++);
    char carryValue = cpu->flags &= 0x01;  //C è in posizione 0. quindi AND con 1 per trovare il valore
    
    //uso short nel caso fosse > 255
    short result = cpu->a + value + carryValue;

    char newCarryValue;
    char overflowValue;         //Note: overflow in this case is equal of Carry. When a Carry is set, basically there is Overflow also.
    if (result > 255) {
        result = result - 255;  //Roll around 255 and Set Carry Flag
        newCarryValue = 1;    
        overflowValue = 1;
    } else {
       newCarryValue = 0;     //Clear Carry Flag
       overflowValue = 0;    
    }

    cpu->a = (char)result;
    M6502_S(cpu, cpu->a >> 7);
    M6502_V(cpu, overflowValue);
    M6502_Z(cpu, cpu->a == 0);
    M6502_C(cpu, newCarryValue);
    return 2;
}

void adc_init(mos6502_t *cpu) {
    cpu->op_codes[0x69] = adc_immediate;
}