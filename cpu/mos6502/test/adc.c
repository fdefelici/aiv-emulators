#include "test.h"
#include <stdio.h>

/* CASE1: Adding 1 to accumulator A = 0. 
 *        With FLAGS: No Carry, No Overflow, No Zero, No Sign
*/
static int test_adc_immediate_case1()
{
    unsigned char rom[] = {0x69, 0x01};

    mos6502_t cpu;
    mos6502_init(&cpu);
    mos6502_add_test_full_mapping(&cpu, rom);

    cpu.a = 0x00;
    cpu.pc = 0;

    int ticks = mos6502_tick(&cpu);

    assert_is_equal(ticks, 2);
    assert_is_equal(cpu.pc, 2);
    assert_is_zero(cpu.flags >> 7); //Sign
    assert_is_zero(((cpu.flags >> 6) & 0x01)); //Overflow
    assert_is_zero(((cpu.flags >> 1) & 0x01)); //Zero
    assert_is_zero((cpu.flags & 0x01)); //Carry
    assert_is_equal(cpu.a, 1);

    return 0;
}

/* CASE2: Adding 10 to accumulator A = 255. 
 *        With FLAGS: No Carry, No Overflow, No Zero, With Sign
*/
static int test_adc_immediate_case2()
{
    unsigned char rom[] = {0x69, 0x0A};

    mos6502_t cpu;
    mos6502_init(&cpu);
    mos6502_add_test_full_mapping(&cpu, rom);

    cpu.a = 0xFF;
    cpu.pc = 0;

    int ticks = mos6502_tick(&cpu);

    assert_is_equal(ticks, 2);
    assert_is_equal(cpu.pc, 2);
    assert_is_zero(cpu.flags >> 7); //Sign
    assert_is_not_zero(((cpu.flags >> 6) & 0x01)); //Overflow
    assert_is_zero(((cpu.flags >> 1) & 0x01)); //Zero
    assert_is_not_zero((cpu.flags & 0x01)); //Carry
    assert_is_equal(cpu.a, 10);

    return 0;
}

int test_all_adc()
{
    test(test_adc_immediate_case1);
    test(test_adc_immediate_case2);
    return 0;
}